// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter', ['ionic','starter.rates'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider){
  //default page that will open when the app start
  $urlRouterProvider.otherwise('/DailyRates');

  //using a global stateProvider:- advantage no need to type again and again
  /**
    example:
    need to use the $stateProvider everytime

  $stateProvider.state('rates',{
    url: '/DailyRates',
    templateUrl: 'templates/rates.html',
    controller: 'ratesCtrl'
  });

  $stateProvider.state('EditRates',{
    url: '/EditRates',
    templateUrl: 'templates/editrates.html',
    controller: 'EditRatesCtrl'
  }); 
  
  ^^semicolan';' is important, only need to put in the last if use global like below
  **/
  $stateProvider

  .state('rates',{
    url: '/DailyRates',
    templateUrl: 'templates/rates.html',
    controller: 'ratesCtrl'
  });
})
